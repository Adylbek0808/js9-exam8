import React, { useEffect, useState } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { Route, Switch } from 'react-router-dom';
import axiosQuotes from '../../axios-quotes';
import EditQuote from '../../components/EditQuote/EditQuote';
import NavLeft from '../../components/NavLeft/NavLeft';
import NavTop from '../../components/NavTop/NavTop';
import Quotes from '../../components/Quotes/Quotes';



const CATEGORIES = [
    { title: "Star Wars", id: "star-wars" },
    { title: "Batman", id: "batman" },
    { title: "Evil Dead", id: "evil-dead" },
    { title: "Forest Gump", id: "forest-gump" },
    { title: "Dr. Strangelove", id: "strangelove" }
]

const BLANK = {
    author: '',
    text: '',
    category: ''
}

const MainPage = props => {

    const [quote, setQuote] = useState({});
    const [quotesList, setQuotesList] = useState([])

    const flipPage = link => {
        props.history.push({
            pathname: '/' + link
        });
    };

    const quoteCategoryChanged = e => {
        const category = e.target.value;
        const catID = CATEGORIES.filter(item => item.title === category);
        setQuote({
            ...quote,
            category: catID[0].id,
            catTitle: category
        });
    };
    const quoteInfoChanged = e => {
        const { name, value } = e.target;
        setQuote({
            ...quote,
            [name]: value
        });
    };

    const sendNewQuote = async e => {
        e.preventDefault();
        const newQuote = quote;
        try {
            await axiosQuotes.post('/quotes.json', newQuote);
            setQuote();
        } finally {
            flipPage('');
        };
    };

    const editQuote = link => {
        flipPage('quotes/' + link + '/edit');
    }


    const fetchQuoteData = async (id) => {
        try {
            const response = await axiosQuotes.get('/quotes/' + id + '.json');
            const data = response.data
            setQuote(data)
        } catch (error) {
            console.error("Network error")
        }
    }
    const deleteQuoteData = async (id) => {
        try {
            await axiosQuotes.delete('/quotes/' + id + '.json');
        } catch (error) {
            console.error("Network error")
        } finally {
            flipPage('');
        };
    }


    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await axiosQuotes.get('/quotes.json');
                const data = Object.keys(response.data).map(item => {
                    const id = item;
                    const quote = {
                        ...response.data[item],
                        id: id
                    }
                    return quote
                });
                console.log(data)
                setQuotesList(data)
            } catch (error) {
                console.error("Network error")
            }
        }
        if (props.history.location.pathname === "/") {
            fetchData().catch(console.error)
        }

    }, [props.history.location.pathname])

    console.log(props)

    return (
        <>
            <NavTop flipPage={flipPage} />
            <Container fluid>
                <Row>
                    <Col xs={3}>
                        <NavLeft flipPage={flipPage} categories={CATEGORIES} />
                    </Col>
                    <Col>
                        <Switch>
                            <Route
                                path="/add-quote" exact
                                render={props => (
                                    <EditQuote
                                        formTitle="Add new quote" btnType="Save"
                                        {...props}
                                        quoteInfo={BLANK}
                                        categories={CATEGORIES}
                                        quoteInfoChanged={quoteInfoChanged}
                                        quoteCategoryChanged={quoteCategoryChanged}
                                        submitInfo={sendNewQuote}
                                    />
                                )}
                            />
                            <Route
                                path="/" exact
                                render={props => (
                                    <Quotes
                                        {...props} title="All quotes"
                                        quotesList={quotesList}
                                        editQuote={editQuote}
                                        deleteQuote={deleteQuoteData}
                                    />
                                )}
                            />
                            <Route
                                path="/quotes/:id/edit"
                                render={props => (
                                    <EditQuote
                                        formTitle="Edit quote" btnType="Save"
                                        {...props}
                                        quoteInfo={quote}
                                        categories={CATEGORIES}
                                        quoteInfoChanged={quoteInfoChanged}
                                        quoteCategoryChanged={quoteCategoryChanged}
                                        fetchQuoteData={fetchQuoteData}
                                    />
                                )}
                            />
                        </Switch>
                    </Col>
                </Row>

            </Container>

        </>
    );
};

export default MainPage;