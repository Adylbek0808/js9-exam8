import axios from 'axios';

const axiosQuotes = axios.create({
    baseURL: 'https://js9-burgeradylbek-default-rtdb.firebaseio.com/js9exam8'
});

export default axiosQuotes;