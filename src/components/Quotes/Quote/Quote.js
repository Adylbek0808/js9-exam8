import React from 'react';
import { Button, Card, Container } from 'react-bootstrap';

const Quote = props => {
    // console.log(props)
    return (
        <Container>
            <Card className="mb-3" >
                <Card.Body>
                    <Card.Title>{props.text}</Card.Title>
                    <Card.Text>----     {props.author}</Card.Text>
                    <Button variant="primary" className="mr-3" onClick={props.editQuote}>Edit</Button>
                    <Button variant="danger" onClick={props.deleteQuote}>Delete</Button>
                </Card.Body>
            </Card>
        </Container>
    );
};

export default Quote;