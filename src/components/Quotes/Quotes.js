import React from 'react';
import { Container } from 'react-bootstrap';
import Quote from './Quote/Quote';

const Quotes = props => {
    const quotesList = props.quotesList.map(item => (
        <Quote
            {...props}
            key={item.id}
            text={item.text}
            author={item.author}
            editQuote={() => props.editQuote(item.id)}
            deleteQuote={() => props.deleteQuote(item.id)}
        />
    ))

    return (
        <Container className="p-3">
            <h1>{props.title}</h1>
            {quotesList}
        </Container>
    );
};

export default Quotes;