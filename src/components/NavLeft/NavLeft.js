import React from 'react';
import { Nav } from 'react-bootstrap';

const NavLeft = props => {
    const MenuItems = props.categories.map(item => (
        <Nav.Link key={item.id} onClick={() => props.flipPage("quotes/" + item.id)} >{item.title}</Nav.Link>
    ));
    return (
        <Nav className="flex-column">
            <Nav.Link onClick={() => props.flipPage('')}>All</Nav.Link>
            {MenuItems}
        </Nav>
    );
};

export default NavLeft;