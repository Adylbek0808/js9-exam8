import { Button } from 'react-bootstrap';
import React, { useEffect } from 'react';
import { Container, Form } from 'react-bootstrap';

// const CATEGORIES = [
//     { title: "Star Wars", id: "star-wars" },
//     { title: "Batman", id: "batman" },
//     { title: "Evil Dead", id: "evil-dead" },
//     { title: "Forest Gump", id: "forest-gump" },
//     { title: "Dr. Strangelove", id: "strangelove" }
// ]

const EditQuote = props => {

    const Options = props.categories.map(item => (
        <option key={item.id}>{item.title}</option>
    ));


    useEffect(() => {
        if (props.match.params.id) {
            console.log(props.match.params.id)
            props.fetchQuoteData(props.match.params.id)
        }
    }, [props.match.params.id])



    return (
        <Container>
            <h1>{props.formTitle}</h1>
            <Form onSubmit={props.submitInfo}>
                <Form.Group>
                    <Form.Label>Category:</Form.Label>
                    <Form.Control
                        as="select"
                        name="category"
                        onChange={props.quoteCategoryChanged}
                        defaultValue={props.quoteInfo.catTitle}
                    >
                        <option></option>
                        {Options}
                    </Form.Control>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Author:</Form.Label>
                    <Form.Control
                        type="text"
                        name="author"
                        onChange={props.quoteInfoChanged}
                        defaultValue={props.quoteInfo.author}
                    />

                </Form.Group>
                <Form.Group>
                    <Form.Label>Quote text:</Form.Label>
                    <Form.Control
                        as="textarea" rows={5}
                        name="text"
                        onChange={props.quoteInfoChanged}
                        defaultValue={props.quoteInfo.text}
                    />

                </Form.Group>
                <Button variant="primary" type="submit">{props.btnType}</Button>
            </Form>
        </Container>
    );
};

export default EditQuote;