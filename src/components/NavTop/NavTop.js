import React from 'react';
import { Nav, Navbar } from 'react-bootstrap';

const NavTop = props => {
    return (
        <Navbar bg="light" expand="lg">
        <Navbar.Brand href="/">Quotes Central</Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse >
            <Nav className="ml-auto">
                <Nav.Link onClick={() => props.flipPage('')}>Quotes</Nav.Link>
                <Nav.Link onClick={() => props.flipPage('add-quote')}>Submit new quote</Nav.Link>
            </Nav>
        </Navbar.Collapse>
    </Navbar>
    );
};

export default NavTop;